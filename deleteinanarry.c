#include <stdio.h>
int main()
{
    int i,n,pos,arr[10];
    
    printf("Enter the size of the array\n");
    scanf("%d",&n);
printf("Enter the elements of the array\n");
    for(i=0;i<n;i++)
        scanf("%d",&arr[i]);
    printf("Enter the position from which the number has to be deleted\n");
    scanf("%d",&pos);
    
    for(i=pos;i<n;i++)
        arr[i]=arr[i+1];
    n--;
    printf("The array after deletion is:\n");
    for(i=0;i<n;i++)
        printf("Arr[%d]=%d\n",i,arr[i]);
    return 0;
}